from flask import Flask, render_template, url_for
app = Flask(__name__)

posts=[
    {
        'author': 'Mario Turco',
        'title': 'Primo post',
        'content': 'Primo contenuto postato',
        'date_posted': '21 Febbraio, 2018'
    },
    {
        'author': 'Mario Turco 2 ',
        'title': 'Secondo post',
        'content': 'Secondo contenuto postato',
        'date_posted': '21 Febbraio, 2018'
    },
]
@app.route("/")
@app.route("/home")
def home():
    return render_template('home.html', posts=posts)

@app.route("/about")
def about():
    return render_template('about.html', title='PROVA')

if __name__ == '__main__':
    app.run(debug=True)
